﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyCalculator
{
    public enum CalcOperation
    {
        Null,
        Addition,
        Subtraction,
        Degree,
        Multiplication
    }

    public partial class Form1 : Form
    {
        private double firstOperator, secondOperator;
        private CalcOperation operation;


        public Form1()
        {
            InitializeComponent();

            firstOperator = secondOperator = 0;
            operation = CalcOperation.Null;
        }

        #region Цифры от 0 до 9
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button3.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button4.Text;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button5.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button6.Text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button7.Text;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button8.Text;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                textBoxInput.Clear();
            }

            textBoxInput.Text += button9.Text;
        }
        #endregion

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxInput.Clear();
        }

        private void buttonRemoveChar_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength > 0)
            {
                textBoxInput.Text = textBoxInput.Text.Substring(0, textBoxInput.TextLength - 1);
            }
            else
            {
                return;
            }
        }

        private void buttonComma_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 0)
            {
                textBoxInput.Text += "0,";
            }
            else if (!textBoxInput.Text.Contains(','))
            {
                textBoxInput.Text += ",";
            }
            else
            {
                return;
            }
        }

        private void buttonZero_Click(object sender, EventArgs e)
        {
            if (textBoxInput.Text.StartsWith("0") && textBoxInput.TextLength == 1)
            {
                return;
            }
            
            textBoxInput.Text += "0";
        }


        private void buttonAddition_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 0)
            {
                return;
            }

            try
            {
                firstOperator = Convert.ToDouble(textBoxInput.Text);
            }
            catch (FormatException exc)
            {

                MessageBox.Show(exc.Message);
                textBoxInput.Clear();
                return;
            }

            textBoxInput.Clear();

            operation = CalcOperation.Addition;
        }

        private void buttonSubtraction_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 0)
            {
                return;
            }

            try
            {
                firstOperator = Convert.ToDouble(textBoxInput.Text);
            }
            catch (FormatException exc)
            {

                MessageBox.Show(exc.Message);
                textBoxInput.Clear();
                return;
            }

            textBoxInput.Clear();

            operation = CalcOperation.Subtraction;
        }

        private void buttonDegree_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 0)
            {
                return;
            }

            try
            {
                firstOperator = Convert.ToDouble(textBoxInput.Text);
            }
            catch (FormatException exc)
            {

                MessageBox.Show(exc.Message);
                textBoxInput.Clear();
                return;
            }

            textBoxInput.Clear();

            operation = CalcOperation.Degree;
        }

        private void buttonMultiplication_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 0)
            {
                return;
            }

            try
            {
                firstOperator = Convert.ToDouble(textBoxInput.Text);
            }
            catch (FormatException exc)
            {

                MessageBox.Show(exc.Message);
                textBoxInput.Clear();
                return;
            }

            textBoxInput.Clear();

            operation = CalcOperation.Multiplication;
        }

        private void buttonEqually_Click(object sender, EventArgs e)
        {
            if (textBoxInput.TextLength == 0)
            {
                return;
            }

            try
            {
                secondOperator = Convert.ToDouble(textBoxInput.Text);
            }
            catch (FormatException exc)
            {

                MessageBox.Show(exc.Message);
                textBoxInput.Clear();
                operation = CalcOperation.Null;
                return;
            }

            switch (operation)
            {
                case CalcOperation.Null:
                    return;
                case CalcOperation.Addition:
                    textBoxInput.Clear();
                    textBoxInput.Text = Convert.ToString(firstOperator + secondOperator);
                    break;
                case CalcOperation.Subtraction:
                    textBoxInput.Clear();
                    textBoxInput.Text = Convert.ToString(firstOperator - secondOperator);
                    break;
                case CalcOperation.Degree:
                    textBoxInput.Clear();

                    try
                    {
                        if (secondOperator == 0)
                        {
                            throw new Exception("Деление на ноль!");
                        }

                        textBoxInput.Text = Convert.ToString(firstOperator / secondOperator);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);

                        operation = CalcOperation.Null;
                        return;
                    }
                    break;
                case CalcOperation.Multiplication:
                    textBoxInput.Clear();
                    textBoxInput.Text = Convert.ToString(firstOperator * secondOperator);
                    break;
            }

            operation = CalcOperation.Null;
        }

    }
}
